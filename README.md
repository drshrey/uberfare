Description:

Input your start address and finish address
Return how you rank among other cities UBER is available in, according to price estimate

High Level Algorithm:

get all variables from user
convert variables into longitude and latitude 
forloop: (iterating through list of cities)
	set var to random point in lat/long in city
	set var to same distance in original from random point --> also in lat/ long
	get response from the two above variables
	get price estimate from response
	create object with properties being name of city and price estimate
	throw object into an array
endloop
sort array according to price estimate property in object, from lowest to highest
find user's city
return index value \

