import json
from project import app
from project.models.Index import Location, Endpoints, EvolvedEndpoints
from flask import render_template, flash, request, jsonify
from wtforms import TextField, validators
from flask.ext.wtf import Form

class InputForm(Form):
	start = TextField("Starting Address", [validators.Length(min=1, max=100)])
	end = TextField("Ending Address", [validators.Length(min=1, max=100)])

#Prints to index page.
#Takes in a PricedRoute object
#Still need to implement database integration
def printToIndex(current_priced_route):
	flash(current_priced_route.route);
	for estimate in current_priced_route.price_data:
		flash(estimate)

@app.route('/', methods=['GET', 'POST'])
def index():
	return render_template("index.html")

@app.route('/endpoints', methods=['GET', 'POST'])
def get_endpoints():
	start_location = Location(request.args.get('start'))
	end_location = Location(request.args.get('end'))
	route = Endpoints(start_location, end_location)
	endpoints = EvolvedEndpoints(route, "https://api.uber.com/v1/estimates/price", "4QjvvxAtnjUgG8g8ThnGcIlxa5gpjbmAwgKzLgUn")
	endpoints.fill_estimates()
	serialized_endpoints = endpoints.serialize()
	jsoned_endpoints = json.dumps(serialized_endpoints)
	start = serialized_endpoints["route"]["start_location"]
	end = serialized_endpoints["route"]["end_location"]
	estimates = serialized_endpoints["estimates"]
	print_endpoints = start["address"] + " --> " + end["address"] + "\n"
	print_estimates = ""
	for estimate in estimates:
		tmp = "Service:" + str(estimate["service"])  + "\t Low Estimate: " + str(estimate["low_estimate"])  + " \t High Estimate: " + str(estimate["high_estimate"])  + "\n"
		print_estimates +=tmp
	print "Endpoints:",print_endpoints
	print "Estimates: ", estimates
	return jsonify(
		result = jsoned_endpoints
	)
