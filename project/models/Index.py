from flask import flash, jsonify
from pygeocoder import Geocoder
import requests
import json

#Classes to be used in the index_controller

'''
Object used to store start and ending location objects
'''
class Endpoints(object):
	def __init__(self, start_location, end_location):
		self.start_location = start_location #Location obj
        	self.end_location = end_location  #Location obj

	def serialize(self):
		return {
				"start_location":self.start_location.serialize(),
				"end_location":self.end_location.serialize()
				}

'''
Stores the address, latitude and longitude
'''
class Location(object):
	def __init__(self, address):
		self.address = address #String
        	#Filling in latitude and longitude data
		results = Geocoder.geocode(self.address)
		self.latitude = results.latitude #float
		self.longitude = results.longitude #float

	def serialize(self):
		return {
				"address":self.address,
				"latitude":self.latitude,
				"longitude":self.longitude
				}
'''
Stores high estimate, low estimate and type of product. To
be used in conjunction with Route in PricedRoute objects
'''
class PriceEstimates(object):
    def __init__(self):
        self.high_estimate = None #int
        self.low_estimate  = None #int
        self.service= None #String

    def serialize(self):
	    return {
			    "service":self.service,
			    "low_estimate":self.low_estimate,
			    "high_estimate":self.high_estimate
			    }

'''
Stores Endpoints object and PriceData object(s) corresponding to Route;
endpoints url and auth for added abstraction
'''
class EvolvedEndpoints(object): #Change name of object
    def __init__(self, route,url="",auth=""):
        self.route = route
        self.historical_estimates = []
        self.url = url
        self.auth = auth

    def serialize(self):
        #seriliaze historical_estimates
	    estimates = []
	    for estimate in self.historical_estimates:
		    estimates.append(estimate.serialize())

	    return {
			    "route":self.route.serialize(),
			    "estimates":estimates
			    }

    #Validates auth
    def is_auth(self):
        if len(self.auth) > 0:
            return True
        return False

    #Validates url
    def is_url(self):
        if len(self.url) > 0:
            return True
        return False

    #Checks out route object, fills price_data appropriately
    #and returns True once finished successfully; uses Uber API
    def fill_estimates(self):
        url = self.url
        payload = {
            "start_latitude":self.route.start_location.latitude,
            "start_longitude":self.route.start_location.longitude,
            "end_latitude":self.route.end_location.latitude,
            "end_longitude":self.route.end_location.longitude,
        }
        response = requests.get(url, params=payload, headers={"Authorization":"Token " + self.auth})
        response = json.dumps(response.json(), skipkeys=False)
        response = json.loads(response)
        print response
        prices = response[u'prices']
        #Iterate through the different products

	for price in prices:
            price_est = PriceEstimates()
	    try:
	    	price_est.high_estimate = int(price[u'high_estimate'])
	        price_est.low_estimate = int(price[u'low_estimate'])
	    except TypeError:
		price_est.high_estimate = "No fare"
		price_est.low_estimate = "No fare"
	    price_est.service = price[u'localized_display_name']
            self.historical_estimates.append(price_est)
        return True
