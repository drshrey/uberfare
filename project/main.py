import fare_func
import sys
import pygeolib
import json

if __name__ == "__main__":
    try:
        address = raw_input("Enter your address..\n")
        #address = "18 Rembrandt Way, East Windsor, NJ"
    except pygeolib.GeocoderError:
        print "Not a valid address"
        sys.exit()

    #Parse address to confirm validity
    valid_address = fare_func.is_address(address)
    if valid_address == False:
        print "Not a valid address"
        sys.exit()

    #Convert address to longitude and latitude coordinates
    start_address = fare_func.get_lat_and_long(address)

    try:
        #address = "10 Bartlett St, New Brunswick, NJ"
        address = raw_input("Enter ending address...\n")
    except pygeolib.GeocoderError:
        print "Not a valid address"
        sys.exit()

    valid_address = fare_func.is_address(address)
    if valid_address == False:
        print "Not a valid address"
        sys.exit()

    end_address = fare_func.get_lat_and_long(address)

    #Get prices data from inputting start and ending Location object
    uber = fare_func.get_prices_data(start_address, end_address)
    uber = json.dumps(uber.json(),skipkeys=False)
    uber = json.loads(uber)
    print uber

    price = uber[u'prices']
    price_estimate = (int(price[0][u'high_estimate']) + int(price[0][u'low_estimate'])) /2

    print price_estimate

    #print fare_func.get_cities('city_list.txt')
